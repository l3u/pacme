# pacme

This is "pacman made easy", a simple Bash wrapper script for [Arch Linux](https://archlinux.org/)'s `pacman` command. I wrote it for easier maintainment of my [Artix Linux](https://artixlinux.org/) machines.

I think that `pacman` has a quite unpleasant syntax. So this script "translates" literal commands to pacman's command line parameters.

E.g. to list all files owned by a package, you type

    pacme list <package>

instead of

    pacman -Qlq <package>

or if you want to remove unneeded dependecies, you type

    pacme depclean

instead of

    pacman -Qtdq | pacman -Rns -

Maybe, someone may find this useful.
